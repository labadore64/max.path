﻿
namespace max.options.optiongroup
{
    /// <summary>
    /// This class contains all the label based options for max.maxseer.
    /// </summary>
    public sealed class PathOptions
    {
        /// <summary>
        /// <para>The maximum number of nodes that the pathfinding engine will use at any time.</para>
        /// </summary>
        /// <value>Quantity</value>
        public int MaxNodes { get; set; } = 50000;
        /// <summary>
        /// <para>The distance between nodes for pathfinding.</para>
        /// <para>The higher this value, the less nodes are created and less accurate the pathfinding.</para>
        /// </summary>
        /// <value>Distance</value>
        public int NodeSpacing { get; set; } = 15;

        /// <summary>
        /// <para>The map width in pixels.</para>
        /// </summary>
        /// <value>Pixels</value>
        public int Width { get; set; } = 800;
        /// <summary>
        /// <para>The map height in pixels.</para>
        /// </summary>
        /// <value>Pixels</value>
        public int Height { get; set; } = 600;
    }
}
