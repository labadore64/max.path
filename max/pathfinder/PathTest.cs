﻿using System;
using System.Collections.Generic;
using max.geometry.shape2D;
using max.options.optiongroup;
using Microsoft.Xna.Framework;

namespace max.path.pathfinder
{
    public class PathTest
    {
        PathOptions Options = new PathOptions();

        /// <summary>The list of nodes on the map.</summary>
        List<PathTestNode> Nodes = new List<PathTestNode>();

        /// <summary>The temporary holding list for calculating pathfinding.</summary>
        List<PathTestNode> TestNodes = new List<PathTestNode>();

        /// <summary>Counts how many nodes have been added.</summary>
        private int NodeCounter = 0;

        /// <summary>The starting position node.</summary>
        private PathTestNode StartNode;
        /// <summary>The destination position node.</summary>
        private PathTestNode EndNode;

        /// <summary>This signals that the pathfinding is completed.</summary>
        private bool Completed;

        private readonly float EPSILON = .01f;

        /// <summary>Whether or not diagonal paths should be tested for.</summary>
        public bool TestDiagonal { get; set; }

        public Polygon2D[] Collisions { get; set; }

        public PathTest(Polygon2D[] collisions)
        {
            Collisions = collisions;

        }

        public PathTest(Polygon2D[] collisions, PathOptions options)
        {
            Collisions = collisions;
            Options = options;
        }

        /// <summary>Adds a node.</summary>
        /// <param name="position">The position of the node to place</param>
        public PathTestNode AddNode(Vector2 position)
        {
            PathTestNode returner = new PathTestNode(position, Collisions,this);
            if (returner.Destroyed)
            {
                return null;
            }
            else
            {
                Nodes.Add(returner);
                NodeCounter++;
                return returner;
            }
        }

        /// <summary>Clear the node list.</summary>
        public void ClearNodes()
        {
            Nodes.Clear();
            NodeCounter = 0;
            Completed = false;
        }
        /// <summary>Calculates the shortest path between two points.</summary>
        /// <param name="p1">Starting point</param>
        /// <param name="p2">Ending point</param>
        public List<Vector2> GetPathBetweenPoints(Vector2 p1, Vector2 p2)
        {
            //initialize the shortest distances for all nodes.
            SetNodes();

            //gets a new path that has the new start and end node
            List<PathTestNode> nodeList = new List<PathTestNode>(Nodes);

            for (int i = 0; i < nodeList.Count; i++)
            {
                nodeList[i].InitShortestDistance();
            }

            EndNode = AddTestNode(nodeList, p2);
            EndNode.ShortestNode = EndNode.Neighbors[0];

            StartNode = AddTestNode(nodeList, p1);
            StartNode.ShortestDistance = 0;

            //add a variable to this and make it so that 
            //it is converted into vector3s
            Nodes = nodeList;
            return GetOptimizedPath(nodeList);
        }

        /// <summary>Adds a test node for pathfinding.</summary>
        private  PathTestNode AddTestNode(List<PathTestNode> list, Vector2 test)
        {
            PathTestNode n = new PathTestNode(test,Collisions,this);
            n.AddNode(GetClosestNode(n));
            list.Add(n);
            return n;
        }

        /// <summary>This generates the node list for the map. It will only produce nodes in places that are not inside collision polygons.</summary>
        public  void SetNodes()
        {
            //clears out the nodes just in case its not empty.
            ClearNodes();

            PathTestNode[,] nodePos = new PathTestNode
                                        [(int)Math.Ceiling((double)(Options.Width / Options.NodeSpacing)),
                                        (int)Math.Ceiling((double)(Options.Height / Options.NodeSpacing))];


            int newWidth = nodePos.GetLength(0)-1;
            int newLength = nodePos.GetLength(1)-1;

            //start at 0,0 and go out to max dist x and max dist y.
            for (int i = 0; i < Options.Width; i += Options.NodeSpacing)
            {
                for(int j = 0; j < Options.Height; j += Options.NodeSpacing)
                {
                    nodePos[Math.Min(i / Options.NodeSpacing,newWidth),Math.Min(j/Options.NodeSpacing,newLength)] = AddNode(new Vector2(i, j));
                }
            }

            //loop again to make connections
            for (int i = 0; i < newWidth; i++)
            {
                for (int j = 0; j < newLength; j++)
                {
                    if (nodePos[i, j] != null)
                    {
                        if (i > 0)
                        {
                            
                            nodePos[i, j].AddNode(nodePos[i - 1, j]);
                            if (TestDiagonal)
                            {
                                if (j > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j - 1]);
                                }
                                if (j < newLength - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j + 1]);
                                }
                            }
                        }
                        if (j > 0)
                        {
                            nodePos[i, j].AddNode(nodePos[i, j - 1]);
                            if (TestDiagonal)
                            {
                                if (i > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j - 1]);
                                }
                                if (i < newWidth - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j - 1]);
                                }
                            }
                        }
                        if (i < newWidth - 1)
                        {
                            nodePos[i, j].AddNode(nodePos[i + 1, j]);
                            if (TestDiagonal)
                            {
                                if (j > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j - 1]);
                                }
                                if (j < newLength - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j + 1]);
                                }
                            }
                        }
                        if (j < newLength - 1)
                        {
                            nodePos[i, j].AddNode(nodePos[i, j + 1]);
                            if (TestDiagonal)
                            {
                                if (i > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j + 1]);
                                }
                                if (i < newWidth - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j + 1]);
                                }
                            }
                        }
                    }
                }
            }


            for (int i = 0; i < Nodes.Count; i++)
            {
                //if a node still has no neighbors, find the closest node and give it that neighbor.
                if (Nodes[i].Neighbors.Count == 0)
                {
                    Nodes[i].AddNode(GetClosestNode(Nodes[i]));
                }
            }
        }


        /// <summary>Arranges the nodes to be processed for pathfinding.</summary>
        private  List<Vector2> GetOptimizedPath(List<PathTestNode> n)
        {
            List<Vector2> list = new List<Vector2>();
            TestNodes = new List<PathTestNode>(n);
            List<PathTestNode> EvalNodes = new List<PathTestNode>();
            PathTestNode processMe;

            EvalNodes.Add(StartNode);

            int counter = 0;
            //while there are still nodes to evaluate.
            while (EvalNodes.Count > 0)
            {
                //evaluate the top node
                processMe = EvalNodes[0];
                EvalNodes.RemoveAt(0);
                if(processMe != null)
                {
                    if (NodeInTestNodes(processMe))
                    {
                        processMe.TestDijsktra();
                        for (int i = 0; i < processMe.Neighbors.Count; i++)
                        {
                            if (NodeInTestNodes(processMe.Neighbors[i]) && EvalNodes.IndexOf(processMe.Neighbors[i]) == -1)
                            {
                                EvalNodes.Add(processMe.Neighbors[i]);
                            }
                        }
                    }
                }

                counter++;
            }

            //set this to the destination node
            processMe = EndNode;
            //create the list representing the shortest path
            while(processMe != null)
            {
                list.Add(processMe.Position);
                processMe = processMe.ShortestNode;
            }
            Nodes = n;
            list.Reverse();
            return list;
        }

        /// <summary>Whether a test node is inside the list of test nodes.</summary>
        public  bool NodeInTestNodes(PathTestNode n)
        {
            if (TestNodes.Find(x => x == n) != null)
            {
                return true;
            }

            return false;
        }
        /// <summary>Removes a test node from the list of test nodes.</summary>
        public  void NodeRemoveTestNodes(PathTestNode n)
        {
            TestNodes.Remove(n);
        }

        /// <summary>Determines whether or not pathfinding has completed.</summary>
        public  bool IsComplete()
        {
            return Completed || NodeCounter > Options.MaxNodes;
        }

        /// <summary>Checks whether or not a test node already exists in the test node list.</summary>
        /// <param name="n">The test node to check</param>
        public  bool NodeAlreadyExists(PathTestNode n)
        {
            if (Nodes.Find((PathTestNode obj) => obj.DistanceToNode(n) < Options.NodeSpacing) == null){
                return false;
            }
            return true;
        }
        /// <summary>Gets the closest node to a certain node.</summary>
        /// <param name="n">The node to test the closest node to</param>
        public  PathTestNode GetClosestNode(PathTestNode n)
        {
            float distance = 100000000f;
            float oldDistance;
            PathTestNode returner = null;
            for (int i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i] != n)
                {
                    oldDistance = distance;
                    distance = Math.Min(distance, Nodes[i].DistanceToNode(n));
                    if (Math.Abs(oldDistance - distance) > EPSILON)
                    {
                        returner = Nodes[i];
                    }
                }
            }

            return returner;
        }
    }
}
