﻿using System;
using System.Collections.Generic;
using max.geometry.helper;
using Microsoft.Xna.Framework;

namespace max.path
{
    /// <summary>
    /// This class represents a path in 3D space.
    /// 
    /// A path travels at a constant speed between a list of nodes.
    /// 
    /// A path can either end by looping back to the first node (END_LOOP),
    /// stopping at the end (END_STOP) or restarting at the beginning (END_RESTART).
    /// </summary>
    public class Path
    {
        /// <summary>The current location on the path.</summary>
        Vector2 Position;
        /// <summary>The destination point you are aiming towards.</summary>
        Vector2 GotoPoint;
        /// <summary>The list of points, representing the full path.</summary>
        List<Vector2> PathPoints;

        /// <summary>The index of the last point reached.</summary>
        int PointIndex;
        /// <summary>The speed to move along.</summary>
        /// <value>Speed</value>
        public float Speed { get; set; }

        /// <summary>End path behaviour. Loops back to the first point (by traveling directly to it).</summary>
        /// <value>Loop to first point</value>
        public const int END_LOOP = 0;
        /// <summary>End path behaviour. Stops moving.</summary>
        /// <value>Stop immediately</value>
        public const int END_STOP = 1;
        /// <summary>End path behaviour. Returns to the first point instantly.</summary>
        /// <value>Restart instantly at first point</value>
        public const int END_RESTART = 2;

        /// <summary>Represents a null vector value.</summary>
        private static readonly Vector2 IgnoreMe = Vector2Helper.DUMMY;
        private const float EPSILON = .001f;

        /// <summary>End path behaviour.</summary>
        /// <value>Behaviour</value>
        public int EndBehave { get; set; }
        /// <summary>Whether or not the path is in motion.</summary>
        /// <value>In motion</value>
        public bool Moving { get; protected set; }
        /// <summary>The distance of the entire path.</summary>
        /// <value>Distance</value>
        public float Distance { get; protected set; }
        /// <summary>The distance along the path travelled.</summary>
        /// <value>Distance</value>
        public float TravelledDistance { get; protected set; }

        /// <summary>
        /// Initialize using a path represented as points.
        /// </summary>
        /// <param name="Points">Points</param>
        public Path(List<Vector2> Points)
        {
            UpdatePath(Points);
            PointIndex = 0;
            Speed = 1;
            Position = Vector2.Zero;
            if (Points.Count == 0)
            {
                GotoPoint = Vector2.Zero;
            }
            else
            {
                GotoPoint = Points[0];
            }
            Moving = false;
            CalculateDistance();
        }

        /// <summary>
        /// Initialize using a set of points and a starting position. The starting position doesn't have to be in the points, but it will not be added to the points, either.
        /// </summary>
        /// <param name="Points">Points</param>
        /// <param name="StartPosition">Start Position</param>
        public Path(List<Vector2> Points, Vector2 StartPosition)
        {
            UpdatePath(Points);
            PointIndex = 0;
            Speed = 5;
            Position = StartPosition;
            if (Points.Count == 0)
            {
                GotoPoint = Vector2.Zero;
            }
            else
            {
                GotoPoint = Points[0];
            }
            Moving = false;
            EndBehave = END_LOOP;
            CalculateDistance();
        }
        /// <summary>Calculates the distance of the path.</summary>
        private void CalculateDistance()
        {
            Distance = 0;
            Vector2 LastPoint = PathPoints[0];
            for(int i = 1; i < PathPoints.Count; i++)
            {
                Distance += Vector2.Distance(LastPoint, PathPoints[i]);
                LastPoint = PathPoints[i];
            }
            if(Distance == 0)
            {
                Distance = 1;
            }
        }
        /// <summary>
        /// Sets the position based on percent travelled. Must be a value 0-1.
        /// </summary>
        /// <param name="Percent">Percent value 0-1</param>
        public void SetTravelledPathDistancePercent(float Percent)
        {
            if(Percent >= 0 && Percent <= 1)
            {
                SetTravelledPathDistance(Distance * Percent);
            } else
            {
                throw new NotImplementedException("Should be between 0 and 1.");
            }
        }
        /// <summary>
        /// Sets the position based on the distance from the first point along the path.
        /// </summary>
        /// <param name="Distance">Distance from the beginning of the path</param>
        public void SetTravelledPathDistance(float Distance)
        {
            TravelledDistance = Distance;
            //find the last point travelled
            float test = 0;
            Vector2 LastPoint = PathPoints[0];
            if (Distance < .01)
            {
                Position = PathPoints[0];
                PointIndex = 0;
                GotoPoint = PathPoints[1];
            }
            else
            {
                for (int i = 1; i < PathPoints.Count; i++)
                {
                    test += Vector2.Distance(LastPoint, PathPoints[i]);
                    //if the test is greater than the travelled distance,
                    //the position will be between LastPoint and PathPoints[i]
                    if (test > TravelledDistance)
                    {
                        float diff = test - TravelledDistance;
                        Position = LastPoint;
                        GotoPoint = PathPoints[i];
                        PointIndex = i - 1;
                        UpdatePosition(diff);
                        break;
                    }
                    else
                    {
                        LastPoint = PathPoints[i];
                    }
                }
            }
        }

        /// <summary>Sets the path points to the points provided.</summary>
        /// <param name="Points">The list of points to add to the path</param>
        public void UpdatePath(List<Vector2> Points)
        {
            PathPoints = Points;
        }
        /// <summary>Adds a point to the end of the path.</summary>
        /// <param name="Point">The point to add</param>
        public void AddPoint(Vector2 Point)
        {
            PathPoints.Add(Point);
        }
        /// <summary>Adds points to the end of the path.</summary>
        /// <param name="Points">The points to add</param>
        public void AddPoints(List<Vector2> Points)
        {
            PathPoints.AddRange(Points);
        }
        /// <summary>Clear all points on the path and stop moving.</summary>
        public void ClearPoints()
        {
            PathPoints.Clear();
            StopMoving();
        }
        /// <summary>Stops the path's movement.</summary>
        private void StopMoving()
        {
            Moving = false;
            GotoPoint = IgnoreMe;
        }
        /// <summary>Returns the path to the beginning.</summary>
        private void ReturnToBeginning()
        {
            GotoPoint = PathPoints[0];
        }
        /// <summary>Checks if GoToPoint is null.</summary>
        private bool GotoIsNull()
        {
            if(GotoPoint == IgnoreMe)
            {
                return true;
            }
            return false;
        }
        /// <summary>Moves a certain distance along the path from the current position.</summary>
        /// <param name="Distance">Distance to travel</param>
        public void UpdatePosition(float Distance)
        {
            Vector2 plsgoto = GotoPoint- Position;
            double angle = Math.Atan2(plsgoto.Y, plsgoto.X);

            double xx = Position.X + Math.Cos(angle)* Distance;
            double yy = Position.Y + Math.Sin(angle) * Distance;
            Position = new Vector2((float)xx, (float)yy);
            TravelledDistance += Distance;
        }
        /// <summary>Starts the path's movement.</summary>
        public void Start()
        {
            //only works if there is at least one path point.
            if (PathPoints.Count > 0)
            {
                if (PathPoints.Count > PointIndex)
                {
                    GotoPoint = PathPoints[PointIndex];
                }
                else
                {
                    GotoPoint = PathPoints[0];
                }
                Moving = true;
            }
        }
        /// <summary>Updates the path every cycle.</summary>
        public void Update()
        {
            //only update if goto is not IgnoreMe
            if (!GotoIsNull() && Math.Abs(Speed) > EPSILON)
            {
                float dist = Vector2.Distance(Position, GotoPoint);
                if (dist > Speed)
                {
                    //do normal update.
                    UpdatePosition(Speed);
                }
                else
                {
                    //update position to be the end point.
                    UpdatePosition(dist);
                    PointIndex++;

                    //this should update the next point.
                    //if the point index is less than the path quantity,
                    //just update to the next one.
                    if(PointIndex < PathPoints.Count)
                    {
                        float diff = Speed - dist;
                        GotoPoint = PathPoints[PointIndex];
                        UpdatePosition(diff);
                    }
                    else
                    {
                        PointIndex = 0;
                        TravelledDistance = 0;
                        //this is end path behaviour.
                        if (EndBehave == END_LOOP)
                        {
                            ReturnToBeginning();
                        } else if (EndBehave == END_RESTART)
                        {
                            if (PathPoints.Count > 0)
                            {
                                Position = PathPoints[0];
                                ReturnToBeginning();
                            }
                            else
                            {
                                StopMoving();
                            }
                        }
                        else
                        {
                            StopMoving();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Returns the position of the path.
        /// </summary>
        /// <returns>Position</returns>
        public Vector2 GetPosition()
        {
            return new Vector2(Position.X, Position.Y);
        }
        /// <summary>
        /// Sets the position of the path.
        /// </summary>
        /// <param name="Position">Position</param>
        public void SetPosition(Vector2 Position)
        {
            this.Position = Position;
        }
        /// <summary>
        /// Gets the points representing the path.
        /// </summary>
        /// <returns>Points</returns>
        public List<Vector2> GetPoints()
        {
            List<Vector2> returnPoints = new List<Vector2>();
            for(int i = 0; i < PathPoints.Count; i++)
            {
                returnPoints.Add(PathPoints[i]);
            }

            return returnPoints;
        }

    }
}
